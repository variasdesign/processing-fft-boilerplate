// Importar módulo de sonido nativo de Processing
import processing.sound.*;

// Crear objetos de sonido
// Objeto analizador FFT
FFT fft;
// Objeto analizador volumen general
Amplitude amp;
// Entrada de audio
AudioIn in;
// Filtro de paso alto (para atenuar graves)
//HighPass hp;

// ¿Cuántas bandas para nuestro FFT? (e.g. 2^8 = 256 bandas)
int bands = int(pow(2, 6));

// Matriz para guardar el análisis FFT de las frecuencias
float[] spectrum = new float[bands];
// Variable para guardar el volumen general
float vol;

// Suavizado del análisis de frecuencias
float smoothingFactorSpectrum = 0.25;
float[] smoothSpectrum = new float[bands];

// Suavizado del análisis de volumen general
float smoothingFactorAmp = 0.015;
float smoothAmp;

void setup() {
  // Escribir en la consola todas las entradas y salidas de audio disponibles (útil para debug)
  Sound.list();
  
  // Inicializar dispositivo de entrada de audio
  Sound s = new Sound(this);
  s.inputDevice(0);

  // Inicializar entrada de audio (this, 0 = entrada de micrófono, 1 = monitor of output)
  in = new AudioIn(this, 0);
  // Inicializar analizador FFT (this, número de bandas)
  fft = new FFT(this, bands);
  // Inicializar analizador volumen
  amp = new Amplitude(this);
  // Inicializar filtro de paso alto
  //hp = new HighPass(this);
  //hp.process(in, 10000/bands);

  // Empezar a registrar sonido en la entrada de audio
  in.start();

  // Conectar la entrada de audio a nuestros analizadores (FFT y volumen)
  fft.input(in);
  amp.input(in);

  size(960, 640);
  //fullScreen();
  colorMode(HSB, 360, 100, 100);
}

void draw() {
  // Analizar el audio
  audioAnalysis();

  // Dibuja algo
  drawBackground();
  drawBands();
}

void drawBackground() {
  // Mapear volumen general a tonos azules y morados
  float volHue = map(vol, 0, .75, 210, 270);

  noStroke();
  fill(volHue, 100, 20);
  rect(0, 0, width, height);
}

void drawBands() {
  // Calcular ancho y alto de bandas
  float bandWidth = (width/2)/float(bands);
  float bandHeight = height/8;
  for (int x = -1; x <= 1; x+=2) {
    for (int y = -1; y <=1; y+=2) {
      pushMatrix();
      if (x == 1) {
        translate(width/2 + x, height/2);
      } else {
        translate(width/2, height/2);
      }
      scale(x, y);
      for (int i = 0; i < bands; i++) {
        // Mapear volumen de frecuencias y bandas a tonos azules y morados
        float freqHue = map(smoothSpectrum[i], 0, .5, 210, 330);
        float freqHue2 = map(i, bands, 0, -30, 30);
        // Mapear volumen de frecuencias a saturación
        float freqSat = map(smoothSpectrum[i], 0, .5, 0, 75);
        float freqHeight = map(smoothSpectrum[i], 0, .075, 0, bandHeight);
        freqHeight = constrain(freqHeight, 0, height/2.2);
        fill(freqHue + freqHue2, 100 - freqSat, 100);
        rect(i*bandWidth, 0, bandWidth - 1, freqHeight);
      }
      popMatrix();
    }
  }
}

void audioAnalysis () {
  // Analizar audio y guardar frecuencias en la matriz
  fft.analyze(spectrum);
  for (int i = 0; i < bands; i++) {
    // Sesgar espectro con función polinómica: ax³ + bx² + cx + d
    float polyIndex = (pow(i, 3) + pow(i, 2) + i)/(8*pow(bands, 2))+1;
    float polySpectrum = spectrum[i] * polyIndex;
    smoothSpectrum[i] += (polySpectrum - smoothSpectrum[i]) * smoothingFactorSpectrum;
    //smoothSpectrum[i] += (spectrum[i] - smoothSpectrum[i]) * smoothingFactorSpectrum;
  }
  // Analizar volumen y guardar su valor en la variable
  vol = amp.analyze();
  smoothAmp += (vol - smoothAmp) * smoothingFactorAmp;
}

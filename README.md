# Processing FFT boilerplate 🇬🇧

This an example sketch made in Processing 4 for my OpenSouthCode 2023 talk "Processing, FFT and live visuals". It contains everything necessary to start making Processing sketches that use audio input as data to animate visuals:

* FFT analyzer
* General amplitude analizer
* Audio input (it accepts microphone or output monitoring)

Just load it in Processing and start coding something in the `void draw()` loop function.

## Links

* [Processing Sound library reference](https://processing.org/reference/libraries/sound/index.html)
* [OpenSouthCode talk entry](https://www.opensouthcode.org/conferences/opensouthcode2023/program/proposals/626)
* [OpenSouthCode](https://www.opensouthcode.org)

---

# Processing FFT boilerplate 🇪🇸

Un sketch de ejemplo realizado en Processing 4 para mi charla del OpenSouthCode 2023 titulada "Processing, FFT y visuales en directo". Contiene todo lo necesario para empezar a realizar sketches en Processing que utilicen una entrada de audio como datos para animar visuales.

* Analizador FFT
* Analizador de volumen general
* Entrada de audio (acepta micrófono o monitoreo de la salida)

Tan sencillo como cargarlo en Processing y empezar a programar algo en la función en bucle `void draw()`.

## Enlaces

* [Referencia de la librería de sonido de Processing](https://processing.org/reference/libraries/sound/index.html)
* [Registro de la charla de OpenSouthCode](https://www.opensouthcode.org/conferences/opensouthcode2023/program/proposals/626)
* [OpenSouthCode](https://www.opensouthcode.org)
